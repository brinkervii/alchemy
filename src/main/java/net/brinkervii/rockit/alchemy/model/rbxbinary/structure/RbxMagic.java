package net.brinkervii.rockit.alchemy.model.rbxbinary.structure;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
public @interface RbxMagic {
	String value();
}
