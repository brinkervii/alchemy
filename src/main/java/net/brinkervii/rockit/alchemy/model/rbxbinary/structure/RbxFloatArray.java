package net.brinkervii.rockit.alchemy.model.rbxbinary.structure;

import net.brinkervii.rockit.alchemy.extract.RobloxBinaryInputStream;

import java.io.IOException;
import java.util.ArrayList;

public class RbxFloatArray extends ArrayList<Float> {
	public RbxFloatArray() {

	}

	private RbxFloatArray(InterleavedArray array) {
		for (int i = 0; i < array.getSize(); i++) {
			add(array.getFloat(i));
		}
	}

	public static RbxFloatArray fromStream(RobloxBinaryInputStream in, int instanceCount) throws IOException {
		return new RbxFloatArray(InterleavedArray.fromStream(in, instanceCount, 4));
	}
}
