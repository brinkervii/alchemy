package net.brinkervii.rockit.alchemy.model.rbxbinary.wranglers;

import lombok.extern.slf4j.Slf4j;
import net.brinkervii.rockit.alchemy.extract.RobloxBinaryInputStream;
import net.brinkervii.rockit.alchemy.model.rbxbinary.RbxBinaryDataType;
import net.brinkervii.rockit.alchemy.model.rbxbinary.binarymember.InstanceRecord;
import net.brinkervii.rockit.alchemy.model.rbxbinary.structure.RbxFloatArray;
import net.brinkervii.rockit.alchemy.model.rbxbinary.structure.RbxIntegerArray;
import net.brinkervii.rockit.model.rbx.data.DataType;
import net.brinkervii.rockit.model.rbx.data.UDim2;

import java.io.IOException;

@Slf4j
@Wrangles(RbxBinaryDataType.UDim2)
public final class UDim2Wrangler extends RbxBinaryDataWrangler {
	@Override
	public DataType[] in(RobloxBinaryInputStream in, InstanceRecord instanceRecord) throws WranglingException {
		try {
			final int n = instanceRecord.getNumberOfInstances();
			final var scalesX = RbxFloatArray.fromStream(in, n);
			final var scalesY = RbxFloatArray.fromStream(in, n);
			final var offsetsX = RbxIntegerArray.fromStream(in, n);
			final var offsetsY = RbxIntegerArray.fromStream(in, n);

			return read(new UDim2[n], index -> new UDim2(
					scalesX.get(index),
					offsetsX.get(index),
					scalesY.get(index),
					offsetsY.get(index))
			);
		} catch (IOException e) {
			throw new WranglingException(e);
		}

	}
}
