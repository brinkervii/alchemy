package net.brinkervii.rockit.alchemy.model.rbxbinary.exception;

import java.io.IOException;

public class NoMagicException extends IOException {
	public NoMagicException(String magic) {
		super("No magic available for " + magic);
	}
}
