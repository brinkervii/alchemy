package net.brinkervii.rockit.alchemy.model.rbxbinary.wranglers;

import lombok.extern.slf4j.Slf4j;
import net.brinkervii.rockit.alchemy.extract.RobloxBinaryInputStream;
import net.brinkervii.rockit.alchemy.model.rbxbinary.RbxBinaryDataType;
import net.brinkervii.rockit.alchemy.model.rbxbinary.binarymember.InstanceRecord;
import net.brinkervii.rockit.model.rbx.data.DataType;

@Slf4j
@Wrangles(RbxBinaryDataType.Faces)
public final class FacesWrangler extends RbxBinaryDataWrangler {
	@Override
	public DataType[] in(RobloxBinaryInputStream in, InstanceRecord instanceRecord) throws WranglingException {
		log.warn("Not implemented");
		return new DataType[0];
	}
}
