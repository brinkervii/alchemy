package net.brinkervii.rockit.alchemy.model.rbxml;

import lombok.Data;
import org.jsoup.nodes.Element;

@Data
public class RbxExternal {
	private String value;

	public RbxExternal(Element element) {
		this.value = element.ownText().trim();
	}
}
