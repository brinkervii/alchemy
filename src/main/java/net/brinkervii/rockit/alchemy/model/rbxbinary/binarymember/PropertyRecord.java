package net.brinkervii.rockit.alchemy.model.rbxbinary.binarymember;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import net.brinkervii.rockit.alchemy.extract.RobloxBinaryInputStream;
import net.brinkervii.rockit.alchemy.model.rbxbinary.RbxBinaryDataType;
import net.brinkervii.rockit.alchemy.model.rbxbinary.wranglers.DataWranglers;
import net.brinkervii.rockit.alchemy.model.rbxbinary.wranglers.WranglingException;
import net.brinkervii.rockit.model.rbx.data.DataType;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Objects;

@Slf4j
public final class PropertyRecord extends RbxBinaryMember {
	private final static DataWranglers WRANGLERS = new DataWranglers();

	@Getter
	private int typeId = 0;
	private String name;
	private RbxBinaryDataType dataType = RbxBinaryDataType.Unknown;
	private byte[] data = new byte[0];
	private RobloxBinaryInputStream dataStream = null;
	private ArrayList<DataType> interpretedData = new ArrayList<>();

	public void interpretData(InstanceRecord instanceRecord) {
		if (WRANGLERS.containsKey(dataType)) {
			try {
				this.interpretedData.addAll(Arrays.asList(WRANGLERS.get(dataType).in(dataStream, instanceRecord)));
			} catch (WranglingException e) {
				e.printStackTrace();
			}
		} else {
			log.error("Cannot wrangle data type " + dataType.toString());
		}
	}

	public static PropertyRecord fromStream(RobloxBinaryInputStream in) throws IOException {
		final PropertyRecord record = new PropertyRecord();

		record.typeId = in.readIntLe();
		record.name = in.readRbxString();
		record.dataType = Objects.requireNonNull(RbxBinaryDataType.fromStream(in));
		record.data = in.readRemaining();
		record.dataStream = new RobloxBinaryInputStream(new ByteArrayInputStream(record.data));

		return record;
	}
}
