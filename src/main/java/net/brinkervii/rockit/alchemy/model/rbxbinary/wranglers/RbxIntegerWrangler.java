package net.brinkervii.rockit.alchemy.model.rbxbinary.wranglers;

import lombok.extern.slf4j.Slf4j;
import net.brinkervii.rockit.alchemy.extract.RobloxBinaryInputStream;
import net.brinkervii.rockit.alchemy.model.rbxbinary.RbxBinaryDataType;
import net.brinkervii.rockit.alchemy.model.rbxbinary.binarymember.InstanceRecord;
import net.brinkervii.rockit.alchemy.model.rbxbinary.structure.RbxIntegerArray;
import net.brinkervii.rockit.model.rbx.data.DataType;
import net.brinkervii.rockit.model.rbx.data.RbxInteger;

import java.io.IOException;
import java.util.stream.Collectors;

@Slf4j
@Wrangles(RbxBinaryDataType.Integer)
public class RbxIntegerWrangler extends RbxBinaryDataWrangler {
	@Override
	public DataType[] in(RobloxBinaryInputStream in, InstanceRecord instanceRecord) throws WranglingException {
		try {
			return RbxIntegerArray.fromStream(in, instanceRecord.getNumberOfInstances()).stream()
					.map(RbxInteger::new)
					.collect(Collectors.toList())
					.toArray(new RbxInteger[instanceRecord.getNumberOfInstances()]);
		} catch (IOException e) {
			throw new WranglingException(e);
		}
	}
}
