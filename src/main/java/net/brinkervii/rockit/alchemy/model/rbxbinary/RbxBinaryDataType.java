package net.brinkervii.rockit.alchemy.model.rbxbinary;

import net.brinkervii.rockit.alchemy.extract.RobloxBinaryInputStream;

import java.io.IOException;

public enum RbxBinaryDataType {
	Unknown,
	String,
	Boolean,
	Integer,
	Float,
	Double,
	UDim,
	UDim2,
	Ray,
	Faces,
	Axes,
	BrickColor,
	Color3,
	Vector2,
	Vector3,
	Vector2int16,
	CFrame,
	Quaternion,
	Enum,
	Ref,
	Vector3int16,
	NumberSequence,
	ColorSequence,
	NumberRange,
	Rect,
	PhysicalProperties,
	Color3uint8,
	Int64,
	SharedString;

	RbxBinaryDataType() {

	}

	public static RbxBinaryDataType fromStream(RobloxBinaryInputStream in) throws IOException {
		final int id = (int) in.readByte();

		for (int i = 0; i < values().length; i++) {
			if (i == id)
				return values()[i];
		}

		return null;
	}
}
