package net.brinkervii.rockit.alchemy.model.rbxbinary.wranglers;

import lombok.extern.slf4j.Slf4j;
import net.brinkervii.rockit.alchemy.extract.RobloxBinaryInputStream;
import net.brinkervii.rockit.alchemy.model.rbxbinary.RbxBinaryDataType;
import net.brinkervii.rockit.alchemy.model.rbxbinary.binarymember.InstanceRecord;
import net.brinkervii.rockit.alchemy.model.rbxbinary.structure.RbxFloatArray;
import net.brinkervii.rockit.alchemy.model.rbxbinary.structure.RbxIntegerArray;
import net.brinkervii.rockit.model.rbx.data.DataType;
import net.brinkervii.rockit.model.rbx.data.UDim;

import java.io.IOException;

@Slf4j
@Wrangles(RbxBinaryDataType.UDim)
public final class UDimWrangler extends RbxBinaryDataWrangler {
	@Override
	public DataType[] in(RobloxBinaryInputStream in, InstanceRecord instanceRecord) throws WranglingException {
		try {
			final int n = instanceRecord.getNumberOfInstances();
			final RbxFloatArray scales = RbxFloatArray.fromStream(in, n);
			final RbxIntegerArray offsets = RbxIntegerArray.fromStream(in, n);

			return read(new UDim[n], index -> new UDim(scales.get(index), offsets.get(index)));
		} catch (IOException e) {
			throw new WranglingException(e);
		}
	}
}
