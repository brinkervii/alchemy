package net.brinkervii.rockit.alchemy.model.rbxbinary.wranglers;

import lombok.extern.slf4j.Slf4j;
import net.brinkervii.rockit.alchemy.extract.RobloxBinaryInputStream;
import net.brinkervii.rockit.alchemy.model.rbxbinary.RbxBinaryDataType;
import net.brinkervii.rockit.alchemy.model.rbxbinary.binarymember.InstanceRecord;
import net.brinkervii.rockit.alchemy.model.rbxbinary.structure.RbxFloatArray;
import net.brinkervii.rockit.model.rbx.data.Color3;
import net.brinkervii.rockit.model.rbx.data.DataType;

import java.io.IOException;

@Slf4j
@Wrangles(RbxBinaryDataType.Color3)
public final class Color3Wrangler extends RbxBinaryDataWrangler {
	@Override
	public DataType[] in(RobloxBinaryInputStream in, InstanceRecord instanceRecord) throws WranglingException {
		try {
			final int n = instanceRecord.getNumberOfInstances();
			final var r = RbxFloatArray.fromStream(in, n);
			final var g = RbxFloatArray.fromStream(in, n);
			final var b = RbxFloatArray.fromStream(in, n);

			return read(new Color3[n], index -> new Color3(
					r.get(index),
					b.get(index),
					g.get(index)
			));
		} catch (IOException e) {
			throw new WranglingException(e);
		}
	}
}
