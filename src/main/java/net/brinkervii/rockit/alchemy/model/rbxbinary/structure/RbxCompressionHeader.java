package net.brinkervii.rockit.alchemy.model.rbxbinary.structure;

import lombok.Getter;
import net.brinkervii.rockit.alchemy.extract.RobloxBinaryInputStream;

import java.io.IOException;

public final class RbxCompressionHeader {
	private final static int RESERVED_DATA_LENGTH = 4;
	private final static int MAGIC_LENGTH = 4;

	@Getter
	private String magic = "";
	@Getter
	private int compressedLength = 0;
	@Getter
	private int decompressedLength = 0;
	@Getter
	private byte[] reserved = new byte[RESERVED_DATA_LENGTH];

	public boolean containsCompressedData() {
		return compressedLength > 0;
	}

	public static RbxCompressionHeader fromStream(RobloxBinaryInputStream in) throws IOException {
		final RbxCompressionHeader header = new RbxCompressionHeader();

		header.magic = in.readCString(MAGIC_LENGTH).trim();
		header.compressedLength = in.readIntLe();
		header.decompressedLength = in.readIntLe();
		in.readNBytes(header.reserved, 0, RESERVED_DATA_LENGTH);

		return header;
	}
}
