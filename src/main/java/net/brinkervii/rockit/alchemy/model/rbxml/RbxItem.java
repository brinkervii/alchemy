package net.brinkervii.rockit.alchemy.model.rbxml;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.jsoup.nodes.Element;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;

@Data
@Slf4j
@JsonSerialize
public class RbxItem {
	@JsonIgnore
	private RbxItem parent;
	private String className;
	private String referent;
	private RbxProperties properties = new RbxProperties();
	@JsonIgnore
	private ArrayList<RbxItem> children = new ArrayList<>();

	public RbxItem(Element element) {
		this(element, null);
	}

	public RbxItem(Element element, RbxItem parent) {
		this.parent = parent;

		if (element.hasAttr("class")) {
			this.className = element.attr("class");
		} else {
			log.warn("Item has no class");
		}

		if (element.hasAttr("referent")) {
			this.referent = element.attr("referent");
		} else {
			log.warn("Element has no referent");
		}

		element.children().forEach(elem -> {
			switch (elem.tagName()) {
				case "Item":
					children.add(new RbxItem(elem, this));
					break;

				case "Properties":
					this.properties = new RbxProperties(elem);
					break;

				default:
					log.warn("Unkown tag name: " + elem.tagName());
					break;
			}
		});
	}

	@JsonIgnore
	public Path path() {
		return path("");
	}

	@JsonIgnore
	public Path path(String extension) {
		final LinkedList<RbxItem> list = new LinkedList<>();
		list.add(this);

		while (list.peekLast().parent != null) {
			list.add(list.peekLast().parent);
		}

		final long noNameItemCount = list.stream().filter(item -> !item.getProperties().containsKey("Name")).count();
		if (noNameItemCount > 0) return null;

		Collections.reverse(list);
		final String pathString = String.join(File.separator, list.stream()
				.map(item -> item.getProperties().get("Name"))
				.toArray(String[]::new)
		);

		return Paths.get(extension.isEmpty() ? pathString : pathString + "." + extension);
	}

	@Override
	@JsonIgnore
	public String toString() {
		return String.format("Class = %s, Referent = %s", className, referent);
	}
}
