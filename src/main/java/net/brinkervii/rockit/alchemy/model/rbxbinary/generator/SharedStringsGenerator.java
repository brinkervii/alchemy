package net.brinkervii.rockit.alchemy.model.rbxbinary.generator;

import net.brinkervii.rockit.alchemy.extract.RobloxBinaryInputStream;
import net.brinkervii.rockit.alchemy.model.rbxbinary.binarymember.RbxBinaryMember;
import net.brinkervii.rockit.alchemy.model.rbxbinary.structure.RbxMagic;
import net.brinkervii.rockit.alchemy.model.rbxbinary.binarymember.SharedStrings;
import net.brinkervii.rockit.alchemy.model.rbxbinary.binarymember.SharedStringsMetadata;

import java.io.IOException;

@RbxMagic("SSTR")
public final class SharedStringsGenerator extends RbxBinaryMemberGenerator {
	@Override
	public RbxBinaryMember generateMember(final RobloxBinaryInputStream in) throws IOException {
		final SharedStringsMetadata metadata = SharedStringsMetadata.fromStream(in);
		return SharedStrings.fromStream(in, metadata);
	}
}
