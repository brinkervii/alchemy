package net.brinkervii.rockit.alchemy.model.rbxml;

import org.jsoup.nodes.Element;

import java.util.ArrayList;

public class RbxProperties extends ArrayList<RbxProperty> {
	public RbxProperties() {

	}

	public RbxProperties(Element element) {
		element.children().forEach(elem -> add(new RbxProperty(elem)));
	}

	public boolean containsKey(String key) {
		for (RbxProperty property : this) {
			if (property.getName().equals(key)) {
				return true;
			}
		}

		return false;
	}

	public String get(String key) {
		for (RbxProperty property : this) {
			if (property.getName().equals(key)) {
				return property.getValue();
			}
		}

		return null;
	}
}
