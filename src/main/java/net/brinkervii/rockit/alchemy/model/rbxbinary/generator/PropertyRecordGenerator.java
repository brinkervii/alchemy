package net.brinkervii.rockit.alchemy.model.rbxbinary.generator;

import net.brinkervii.rockit.alchemy.extract.RobloxBinaryInputStream;
import net.brinkervii.rockit.alchemy.model.rbxbinary.binarymember.PropertyRecord;
import net.brinkervii.rockit.alchemy.model.rbxbinary.binarymember.RbxBinaryMember;
import net.brinkervii.rockit.alchemy.model.rbxbinary.structure.RbxMagic;

import java.io.IOException;

@RbxMagic("PROP")
public final class PropertyRecordGenerator extends RbxBinaryMemberGenerator {
	@Override
	public RbxBinaryMember generateMember(RobloxBinaryInputStream in) throws IOException {
		return PropertyRecord.fromStream(in);
	}
}
