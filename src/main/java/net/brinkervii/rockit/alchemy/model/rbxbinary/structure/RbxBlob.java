package net.brinkervii.rockit.alchemy.model.rbxbinary.structure;

import net.brinkervii.rockit.alchemy.extract.RobloxBinaryInputStream;
import net.jpountz.lz4.LZ4Factory;
import net.jpountz.lz4.LZ4FastDecompressor;

import java.io.ByteArrayInputStream;
import java.io.IOException;

public final class RbxBlob {
	private final static LZ4Factory LZ_4_FACTORY = LZ4Factory.fastestInstance();

	private byte[] compressedData = new byte[0];
	private byte[] data = new byte[0];

	public RobloxBinaryInputStream getStream() {
		return new RobloxBinaryInputStream(new ByteArrayInputStream(data));
	}

	public static RbxBlob fromStream(RobloxBinaryInputStream in, RbxCompressionHeader compressionHeader) throws IOException {
		final RbxBlob blob = new RbxBlob();

		if (compressionHeader.containsCompressedData()) {
			blob.compressedData = new byte[compressionHeader.getCompressedLength()];
			blob.data = new byte[compressionHeader.getDecompressedLength()];

			in.readNBytes(blob.compressedData, 0, compressionHeader.getCompressedLength());

			final LZ4FastDecompressor decompressor = LZ_4_FACTORY.fastDecompressor();
			decompressor.decompress(
					blob.compressedData,
					0,
					blob.data,
					0,
					compressionHeader.getDecompressedLength()
			);

			blob.compressedData = new byte[0];
		} else {
			in.readNBytes(blob.data, 0, compressionHeader.getCompressedLength());
		}

		return blob;
	}
}
