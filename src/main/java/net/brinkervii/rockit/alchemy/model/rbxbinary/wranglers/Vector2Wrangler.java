package net.brinkervii.rockit.alchemy.model.rbxbinary.wranglers;

import lombok.extern.slf4j.Slf4j;
import net.brinkervii.rockit.alchemy.extract.RobloxBinaryInputStream;
import net.brinkervii.rockit.alchemy.model.rbxbinary.RbxBinaryDataType;
import net.brinkervii.rockit.alchemy.model.rbxbinary.binarymember.InstanceRecord;
import net.brinkervii.rockit.alchemy.model.rbxbinary.structure.RbxFloatArray;
import net.brinkervii.rockit.model.rbx.data.DataType;
import net.brinkervii.rockit.model.rbx.data.Vector2;

import java.io.IOException;

@Slf4j
@Wrangles(RbxBinaryDataType.Vector2)
public final class Vector2Wrangler extends RbxBinaryDataWrangler {
	@Override
	public DataType[] in(RobloxBinaryInputStream in, InstanceRecord instanceRecord) throws WranglingException {
		try {
			final int n = instanceRecord.getNumberOfInstances();
			final RbxFloatArray x = RbxFloatArray.fromStream(in, n);
			final RbxFloatArray y = RbxFloatArray.fromStream(in, n);

			return read(new Vector2[n], index -> new Vector2(
					x.get(index),
					y.get(index)
			));
		} catch (IOException e) {
			throw new WranglingException(e);
		}
	}
}
