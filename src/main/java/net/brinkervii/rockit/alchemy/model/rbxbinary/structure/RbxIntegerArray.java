package net.brinkervii.rockit.alchemy.model.rbxbinary.structure;

import net.brinkervii.rockit.alchemy.extract.RobloxBinaryInputStream;

import java.io.IOException;
import java.util.ArrayList;

public class RbxIntegerArray extends ArrayList<Integer> {
	public RbxIntegerArray() {

	}

	private RbxIntegerArray(InterleavedArray array) {
		for (int i = 0; i < array.getSize(); i++) {
			add(array.getInt(i));
		}
	}

	public static RbxIntegerArray fromStream(RobloxBinaryInputStream in, int instanceCount) throws IOException {
		return new RbxIntegerArray(InterleavedArray.fromStream(in, instanceCount, 4));
	}
}
