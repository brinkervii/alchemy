package net.brinkervii.rockit.alchemy.model.rbxbinary.binarymember;

import lombok.Getter;
import net.brinkervii.rockit.alchemy.extract.RobloxBinaryInputStream;
import net.brinkervii.rockit.alchemy.model.rbxbinary.structure.ReferentArray;

import java.io.IOException;

public final class InstanceRecord extends RbxBinaryMember {

	@Getter
	private int typeId = 0;
	@Getter
	private String name = "";
	@Getter
	private boolean isService = false;
	@Getter
	private int numberOfInstances = 0;
	@Getter
	private ReferentArray referentArray = null;
	@Getter
	private byte[] serviceFlags = new byte[0];

	public static InstanceRecord fromStream(RobloxBinaryInputStream in) throws IOException {
		final InstanceRecord record = new InstanceRecord();

		record.typeId = in.readIntLe();
		record.name = in.readRbxString();
		record.isService = in.readRbxBoolean();
		record.numberOfInstances = in.readIntLe();
		record.referentArray = ReferentArray.fromStream(in, record.numberOfInstances);

		record.serviceFlags = new byte[record.numberOfInstances];
		in.readNBytes(record.serviceFlags, 0, record.numberOfInstances);

		return record;
	}
}
