package net.brinkervii.rockit.alchemy.model.rbxbinary.binarymember;

import net.brinkervii.rockit.alchemy.extract.RobloxBinaryInputStream;

import java.io.IOException;

public final class RbxBinaryFooter extends RbxBinaryMember {
	private final static int DATA_LENGTH = 21; // Is actually 22, but the leading 0 get lost
	private byte[] data = new byte[DATA_LENGTH];

	public static RbxBinaryFooter fromStream(RobloxBinaryInputStream in) throws IOException {
		final RbxBinaryFooter footer = new RbxBinaryFooter();

		in.readNBytes(footer.data, 0, DATA_LENGTH);

		return footer;
	}
}
