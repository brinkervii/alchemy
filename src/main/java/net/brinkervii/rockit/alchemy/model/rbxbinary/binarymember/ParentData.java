package net.brinkervii.rockit.alchemy.model.rbxbinary.binarymember;

import net.brinkervii.rockit.alchemy.extract.RobloxBinaryInputStream;
import net.brinkervii.rockit.alchemy.model.rbxbinary.structure.ReferentArray;

import java.io.IOException;

public final class ParentData extends RbxBinaryMember {
	private byte format = 0x0;
	private int count = 0;
	private ReferentArray referentArray = null;
	private ReferentArray parentArray = null;

	public static ParentData fromStream(RobloxBinaryInputStream in) throws IOException {
		final ParentData parentData = new ParentData();

		parentData.format = in.readByte();
		parentData.count = in.readIntLe();
		parentData.referentArray = ReferentArray.fromStream(in, parentData.count);
		parentData.parentArray = ReferentArray.fromStream(in, parentData.count);

		return parentData;
	}
}
