package net.brinkervii.rockit.alchemy.extract;

public abstract class OutputFile extends FSTask {
	@Override
	public int priority() {
		return 10;
	}
}
