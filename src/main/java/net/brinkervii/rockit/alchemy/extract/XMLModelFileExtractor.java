package net.brinkervii.rockit.alchemy.extract;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import net.brinkervii.rockit.alchemy.model.rbxml.RbxItem;
import net.brinkervii.rockit.alchemy.model.rbxml.RbxModel;
import net.brinkervii.rockit.alchemy.task.ExtractRobloxFileTaskConfiguration;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.parser.Parser;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.stream.Stream;

@Slf4j
public class XMLModelFileExtractor extends Extractor {
	private final static ObjectMapper OBJECT_MAPPER = new ObjectMapper();
	
	private final FSTaskQueue fsTasks = new FSTaskQueue();

	public XMLModelFileExtractor(ExtractRobloxFileTaskConfiguration configuration) {
		super(configuration);
	}

	@Override
	public void run() {
		try {
			log.info("Parsing document");
			Document document = Jsoup.parse(
					new FileInputStream(configuration.getInputFile()),
					StandardCharsets.UTF_8.toString(),
					"",
					Parser.xmlParser()
			);

			log.info("Translating model");
			final RbxModel model = new RbxModel(document);
			document = null;
			System.gc();

			if (configuration.isExtractScripts()) {
				this.makeScriptTasks(model);
			}

			if (configuration.isDumpInstances()) {
				this.doDumpInstances(model);
			}

			System.gc();

			fsTasks.run();

			try {
				fsTasks.except();
			} catch (Exception e) {
				e.printStackTrace();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private Stream<RbxItem> filterScripts(Stream<RbxItem> inputStream, boolean positive) {
		if (positive) {
			return inputStream
					.filter(item -> item.getClassName().toLowerCase().endsWith("script"))
					.filter(item -> item.getProperties().containsKey("Source"));
		} else {
			return inputStream
					.filter(item -> !item.getClassName().toLowerCase().endsWith("script"))
					.filter(item -> !item.getProperties().containsKey("Source"));
		}
	}

	private void makeScriptTasks(RbxModel model) {
		log.info("Making script dumping tasks");

		filterScripts(model.flatMap(true), true)
				.forEach(item -> emitScript(item, item.path("lua")));
	}

	private void doDumpInstances(RbxModel model) {
		log.info("Making instance dumping tasks");

		filterScripts(model.flatMap(true), false)
				.forEach(item -> emitInstance(item, item.path("inst.json")));
	}

	private void emitInstance(final RbxItem item, Path path) {
		if (path == null) {
			log.error("Path for item was null, not emitting instance");
			return;
		}

		final Path outputPath = preEmit(path);
		synchronized (fsTasks) {
			fsTasks.add(new OutputFile() {
				@Override
				public void run() throws IOException {
					OBJECT_MAPPER.writerWithDefaultPrettyPrinter().writeValue(outputPath.toFile(), item);
				}
			});
		}
	}

	private void emitScript(final RbxItem item, Path path) {
		if (path == null) {
			log.error("Path for item was null, not emitting script");
			return;
		}

		final Path outputPath = preEmit(path);
		synchronized (fsTasks) {
			fsTasks.add(new OutputFile() {
				@Override
				public void run() throws IOException {
					Files.write(outputPath, item.getProperties().get("Source").getBytes(StandardCharsets.UTF_8));
				}
			});
		}
	}

	private Path preEmit(Path path) {
		final Path outputPath = Paths.get(configuration.getOutputDirectory().toString(), path.toString());
		final File outputParent = outputPath.toFile().getParentFile();

		if (!outputParent.exists()) {
			synchronized (fsTasks) {
				fsTasks.add(new CreateDirectory(outputParent));
			}
		}

		return outputPath;
	}
}
