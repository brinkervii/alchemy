package net.brinkervii.rockit.alchemy.extract;

import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.charset.StandardCharsets;

public class RobloxBinaryInputStream extends DataInputStream {
	public RobloxBinaryInputStream(InputStream in) {
		super(in);
	}

	public int readIntLe() throws IOException {
		final byte[] buffer = new byte[4];
		in.readNBytes(buffer, 0, 4);
		return ByteBuffer.wrap(buffer).order(ByteOrder.LITTLE_ENDIAN).getInt();
	}

	public String readCString(final int length) throws IOException {
		final byte[] buffer = new byte[length];
		in.readNBytes(buffer, 0, length);
		return new String(buffer, 0, length, StandardCharsets.US_ASCII);
	}

	public String readRbxString() throws IOException {
		final int length = readIntLe();
		final byte[] buffer = new byte[length];
		in.readNBytes(buffer, 0, length);
		return new String(buffer, 0, length, StandardCharsets.UTF_8);
	}

	public byte[] readRbxStringRaw() throws IOException {
		final int length = readIntLe();

		if (length > 0) {
			final byte[] buffer = new byte[length];
			in.readNBytes(buffer, 0, length);
			return buffer;
		} else {
			return new byte[0];
		}
	}

	public boolean readRbxBoolean() throws IOException {
		return in.read() != 0;
	}

	public byte[] readRemaining() throws IOException {
		final ByteArrayOutputStream bos = new ByteArrayOutputStream();
		int data = read();
		while (data != -1) {
			bos.write(data);
			data = in.read();
		}

		return bos.toByteArray();
	}
}
