package net.brinkervii.rockit.alchemy.extract;

import net.brinkervii.rockit.alchemy.model.rbxbinary.RbxBinaryModel;
import net.brinkervii.rockit.alchemy.task.ExtractRobloxFileTaskConfiguration;

import java.io.IOException;

public class BinaryModelFileExtractor extends Extractor {
	public BinaryModelFileExtractor(ExtractRobloxFileTaskConfiguration configuration) {
		super(configuration);
	}

	@Override
	public void run() {
		try {
			final RbxBinaryModel model = RbxBinaryModel.load(configuration.getInputFile());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
