package net.brinkervii.rockit.model.rbx.data;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public final class UDim2 extends DataType {
	private Float scaleX;
	private Integer offsetX;
	private Float scaleY;
	private Integer offsetY;

	public UDim2(Float scaleX, Integer offsetX, Float scaleY, Integer offsetY) {
		this.scaleX = scaleX;
		this.offsetX = offsetX;
		this.scaleY = scaleY;
		this.offsetY = offsetY;
	}
}
