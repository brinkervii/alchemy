package net.brinkervii.rockit.model.rbx.data;

import lombok.Data;

@Data
public final class RbxString extends DataType {
	private String value;

	public RbxString(String s) {
		this.value = s;
	}
}
