package net.brinkervii.rockit.model.rbx.data;

import lombok.Data;

@Data
public final class RbxInteger extends DataType {
	private Integer value;

	public RbxInteger(Integer value) {
		this.value = value;
	}
}
