package net.brinkervii.rockit.model.rbx.data;

import lombok.Data;

@Data
public final class RbxBoolean extends DataType {
	private boolean value;

	public RbxBoolean(boolean value) {

		this.value = value;
	}
}
