package net.brinkervii.rockit.model.rbx.data;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public final class Ray extends DataType {
	private Vector3 origin;
	private Vector3 direction;

	public Ray(Vector3 origin, Vector3 direction) {
		this.origin = origin;
		this.direction = direction;
	}
}
