package net.brinkervii.rockit.model.rbx.data;

import lombok.Data;

@Data
public final class UDim extends DataType {
	private Float scale;
	private Integer offset;

	public UDim(Float scale, Integer offset) {

		this.scale = scale;
		this.offset = offset;
	}
}
