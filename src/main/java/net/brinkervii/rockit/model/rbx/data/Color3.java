package net.brinkervii.rockit.model.rbx.data;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public final class Color3 extends DataType {
	private Float r;
	private Float g;
	private Float b;

	public Color3(Float r, Float g, Float b) {
		this.r = r;
		this.g = g;
		this.b = b;
	}
}
